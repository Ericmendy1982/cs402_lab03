        .data 0x10010000
var1:    .word 5
var2:    .word 1
var3:    .word -2020

        .text
        .globl main
       
main:
         addu $s0,$ra,$0      # save $31 in $16
         lw $t1, var1        
         lw $t2, var2
         lw $t3, var3
         
         bne $t1,$t2, Else     # if $t1 != $t2, go to Else
         sw  $t3,var1          # make var1==var3, and var2 == var3
         sw  $t3,var2  
         beq $0, $0, Exit        
         
Else:                          # swap var1 and var2
        lw $t4, var1
        sw $t2, var1
        sw $t4, var2
        
        beq $0, $0, Exit
        
Exit:
       li $v0,1               # print new var1
       lw $a0,var1
       syscall

       li $v0,1               # print new var2
       lw $a0,var2
       syscall

       li $v0,10
       syscall
       
       
