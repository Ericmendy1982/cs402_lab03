.data 

prompt1: .asciiz "Please enter an integer:" 
prompt2: .asciiz "I am nearby."
prompt3: .asciiz "I am far."
far: .word 1

     .text
     .globl main

main:
      li $v0,4             # system call for print_str
      la $a0,prompt1
      syscall      
      li $v0,5             # system call for read_int
      syscall
      move $t0,$v0
      
      li $v0,4             # system call for print_str
      la $a0,prompt1
      syscall
      li $v0,5             # system call for read_int
      syscall
      move $t1,$v0
            

      beq $t0,$t1, Far    # if two integers are equal, go to Far
      li $v0,4             # two integers are not equal
      la $a0,prompt2
      syscall
      
      
      li $v0,10
      syscall

Far:                      # two integers are equal              
      li $t8, 0x10010000

      li $v0,4
      la $a0,prompt3
      syscall
      jr $t8             #jump to that far away address
      
      li $v0,10
      syscall
     