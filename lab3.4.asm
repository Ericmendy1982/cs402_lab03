.data 
my_array: .space 40       # reserve 40bytes (10 words) for variable my_array
initial:  .word 5
     .text
     .globl main

main:
      lw $t1, initial     # initial value to $t1
      li $t0, 0           # iterator
      li $a1, 10
      la $a0, my_array
      

Loop:  
      bge $t0,$a1, Exit    # if i>=10, go to Exit 
      sll $t3,$t0,2        #4*i 
      addu $a2,$a0,$t3     # address of register to save my_array[i]    
      sw $t1,($a2)         # save my_array[i]
      addi $t0,$t0,1       #i++
      addi $t1,$t1,1       #j++
      j Loop

Exit:
      li $v0,10
      syscall
     
      
