        .data 0x10010000
var1:    .word 5

        .text
        .globl main
       
main:
         addu $s0,$ra,$0      # save $31 in $16
         lw $t0,var1          # load var1 to $t0
         li $a1, 100          # make $a1==100         
Loop:    ble $a1,$t0,Exit     # if i>=100, exit
         addi $t0,$t0,1       # i=i+1
         sw $t0,var1          # save new value to var1
         j Loop               # loop again
         
        
Exit:  
       li $v0,1               # print new var1
       lw $a0,var1
       syscall


       li $v0,10
       syscall